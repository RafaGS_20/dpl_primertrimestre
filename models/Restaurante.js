/* ------------------------------------------------------- Modelo que define los restaurantes. -------------------------------------------------------*/

// Recursos a usar. 
let mongoose = require('mongoose'); 

// Esquema para la base de datos. 
let Schema = mongoose.Schema; 

let restauranteSchema = new Schema ({

    restauranteID: Number, 

    nombre: String, 

    horario: String, 

    tipocomida: String, 

    telefono: Number,

    ubicacion: { type: [Number], index: true }

});

let Restaurante = function(id, nombre, horario, tipocomida, telefono, ubicacion) {

    this.id = id; 

    this.nombre = nombre;

    this.horario = horario;

    this.tipocomida = tipocomida; 

    this.telefono = telefono; 

    this.ubicacion = ubicacion;

}

// Métodos a usar. 

// De búsqueda: 
restauranteSchema.statics.allRestaurante = function (cb) 
{

    return this.find({}, cb);

}

restauranteSchema.statics.findById = function (id, cb) {

    return this.findById(id, cb);

}

// De alteración de datos: 
restauranteSchema.statics.add = function (aRestaurante, cb) {

    return this.create(aRestaurante, cb); 

}

restauranteSchema.statics.removeById = function (id, cb) {

    return this.findByIdAndDelete(id, cb);

}

restauranteSchema.statics.update = function (idDocumento, documento, cb) {

    return this.findByIdAndUpdate(idDocumento, documento, cb); 

}

// Export. 
module.exports = mongoose.model("Restaurante", restauranteSchema); 