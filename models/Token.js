/* ------------------------------------------------------- Modelo que define los token. -------------------------------------------------------*/



// Base de datos. 
let mongoose = require ("mongoose"); 
let Schema = mongoose.Schema;

// Definimos el esquema con el token. 
let TokenSchema = new Schema ({

    _userId: {

        type: mongoose.Schema.Types.ObjectId,
        required: true, 

        ref: "Usuario"
    }, 

    token: {

        type: String, 

        required: true

    },

    createAt: {

        type: Date, 

        required: true, 

        default: Date.now, 

        expires: 43200
    }

}); 

// Exportamos todo para el controlador y demás. 
module.exports = mongoose.model("Token", TokenSchema); 