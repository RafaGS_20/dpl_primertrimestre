/* ------------------------------------------------------- Modelo que define las reservas. -------------------------------------------------------*/

// Recursos a usar. 
const moment = require("moment"); 
let mongoose = require ("mongoose"); 

// Esquema para la base de datos. 
let Schema = mongoose.Schema; 

let reservaSchema = new Schema ({

    desde: Date, 

    hasta: Date, 

    restaurante: { type: mongoose.Schema.Types.ObjectId, ref: "Restaurante" }, 

    usuario: { type: mongoose.Schema.Types.ObjectId, ref: "Usuario" },

    personas: Number

});

let Reserva = function(desde, restaurante, usuario, personas) {

    this.desde = desde; 

    this.hasta = desde.setHours(desde.getHours() + 2), 

    this.restaurante = restaurante, 

    this.usuario = usuario, 

    this.personas = personas

};

// Busqueda de todas las reservas existentes: 
reservaSchema.statics.reserva = function (cb) {

    return this.find({}, cb); 

}

// Exportamos. 
module.exports = mongoose.model("Rserva", reservaSchema); 