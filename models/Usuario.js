/* ------------------------------------------------------- Modelo que define a los usuarios. -------------------------------------------------------*/

// Otros modelos a usar. 
let Reserva = require("./Reserva"); 
let Token = require("./Token"); 

// Recursos a usar. 
let bcrypt = require ("bcrypt"); 
let uniqueValidator = require("mongoose-unique-validator"); 
let crypto = require("crypto"); 
let mailer = require("../mailer/mailer"); 
let mongoose = require ("mongoose"); 

// Esquema para la base de datos. 
let Schema = mongoose.Schema; 

// Este método se usa en el esquema así que lo declaramos antes. 
let validateEmail = function (email) {

    let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

    return re.test(email);

}

// Definimos el esquema para nuestro usuario. 
let usuarioSchema = new Schema ({

    nombre: {

        type: String,

        trim: true, //Quita los espacios al inicio y al final del string.

        required: [true, "El nombre es obligatorio"] 

    },

    email: {

        type: String,

        trim: true,

        required: [true, "El email es obligatorio"],

        lowercase: true,

        unique: true,

        validate: [validateEmail, "Por favor, introduzca un email válido"],

        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w)*(\.\w{2,4})+$/]

    },

    password: {

        type: String,

        required: [true, "El password es obligatorio"]

    },

    passwordResetToken: String,

    passwordResetTokenExpires: Date,

    verificado: {

        type: Boolean,

        default: false

    }

});

// Nos asegura que el email no se repite entre usuarios. 
usuarioSchema.plugin(uniqueValidator, {message: "El email ya existe con otro usuario."});

// Este método nos permite encriptar las contraseñas. 
usuarioSchema.pre("save", function(next) {

    if (this.isModified("password")) {

        let saltRounds = 10;

        this.password = bcrypt.hashSync(this.password, saltRounds);

    }

    next();

});


// Le asigna el nombre al usuario. 
let Usuario = function (nombre) {

    this.nombre = nombre;

}

// Método que nos permite realizar una reserva con nuestro usuario. 
usuarioSchema.methods.reservar = function(restauranteId, desde, hasta, cb) {
    
    let reserva = new Reserva({usuario: this._id, restaurante: restauranteId, desde: desde, hasta:  hasta});

    console.log(reserva);

    reserva.save(cb);

}

// Permite buscar datos de usuarios a través de los id. 
usuarioSchema.statics.finById = function(id, cb) {

    return this.findById(id, cb);

}

// Permite validar las contraseñas. 
usuarioSchema.methods.validPassword = function (password) {

    return bcrypt.compareSync(password, this.password);

}

// Este método le envía un email de bienvenida a los nuevos usuarios, así como un token para controlar sus sesiones. 
usuarioSchema.methods.enviar_email_bienvenida = function(cb) {

    let token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString("hex")}); // El token es un string en hexadecimal.

    let url = "http://192.168.56.102:3000" + "\/token/confirmation\/" + token.token;

    let email_destination = this.email;

    token.save(function (err) {

        if (err) return console.log(err.message);

        let mailOptions = {

            from: "no-reply@DPL_PrimerTrimestre.com",

            to: email_destination,

            subject: "Verificación de cuenta",

            text: "Hola, \n\n" + "Por favor, para verificar su cuenta haga click en este enlace: \n" + '<a href="' + url + '">' + url + "</a>"

        }

        mailer.sendMail(mailOptions, function(err) {

            if(err) return console.log(err.message);

            console.log("Se ha enviado un email de bienvenida a " + email_destination + ".");

        });

    });

}

// Método usado en caso de ser necesario resetear la contraseña. 
usuarioSchema.methods.resetPassword = function(cb) {

    let token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});

    let email_destination = this.email;

    token.save(function (err) {

        if(err) {return cb(err);}

        let mailOptions = {

            from:"no-reply@DPL_PrimerTrimestre.com",

            to: email_destination,

            subject: "Reseteo de password de cuenta",

            text: "Hola,\n\n" + "Por favor, para resetear el password de su cuenta haga click en este enlace: \n" + "http://192.168.56.102:3000" + "\/resetPassword\/" + token.token + "\n" 

        };

        mailer.sendMail(mailOptions, function (err) {

            if (err) return cb(err);

            console.log("Se ha enviado un email para resetear el password a: " + email_destination + ".");

        });

        cb(null);

    });

}

// Exportamos todo el modelo. 
module.exports = mongoose.model("Usuario", usuarioSchema);