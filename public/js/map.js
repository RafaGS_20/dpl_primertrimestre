var map = L.map('main_map').setView([28.4965, -13.8622], 13);



L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>'  

}).addTo(map);



$.ajax ( {
    // Tipo de dato a usar: 
    dataType: "json",
    // La api a la que tendrá que acceder. 
    url: "api/restaurantes",

    success: function(result){

        console.log(result);
        // Con esto colocaremos los restaurantes en el mapa. 
        result.restaurantes.forEach(function(restaurante){   

            L.marker(restaurante.ubicacion, {title: restaurante.id}).addTo(map);

        });

    }

}); 