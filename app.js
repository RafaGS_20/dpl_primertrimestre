/* ------------------------------------------------------- Nuestra aplicación. -------------------------------------------------------*/

/* Este es el servidor que arrancamos y aquí se encuentra todo lo que necesitamos y las formas en que deben usarse.
Si hay mal aquí toda la aplicación acabará. */


/* ------------------------------------------------------- Le pasamos todos los módulos. -------------------------------------------------------*/
var createError = require('http-errors'); // Gestiona errores HTTP.
var express = require('express'); // Llama a express para su uso. 
var path = require('path'); // Consultar utilidad. 
var cookieParser = require('cookie-parser'); // Gestión de cookies.
var logger = require('morgan'); // Llamamos a morgan para su uso. 
var mongoose = require('mongoose'); // Llamamos a mongoose para su uso.

/* ------------------------------------------------------- Ponemos a funcionar los pasaportes y sesiones. -------------------------------------------------------*/
const passport = require('./config/passport');
const session = require('express-session');

/* ------------------------------------------------------- Ponemos a funcionar los modelos usarios y tokens. -------------------------------------------------------*/
var Usuario = require('./models/Usuario');
var Token = require('./models/Token');

const store = new session.MemoryStore;

/* ------------------------------------------------------- Conectamos con la base de datos. -------------------------------------------------------*/
mongoose.connect('mongodb://localhost/DPL_PrimerTrimestre', {useUnifiedTopology: true, useNewUrlParser: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;

/* Vincular la conexión de un evento de error (para obtener 
notificiaciones de errores en la conexión a la BD) */
db.on("error", console.error.bind('Error de conexión con MongoDB'));

/* ------------------------------------------------------- Enrutadores que vamos a utilizar. -------------------------------------------------------*/
var indexRouter = require('./routes/index'); // Enrutador inicial. 
var usersRouter = require('./routes/users'); // Enrutador si tiras a users. 
// Importo el router para restaurantes y para su API. 
var restaurantesRouter = require('./routes/restaurantes');
var restaurantesAPIRouter = require('./routes/api/restaurante');
// Importo el router de los usuarios así como el de su API. 
var usuariosRouter = require("./routes/usuarios");
var usuariosAPIRouter = require('./routes/api/usuarios');
// Importo el router de la API de las reservas.
var reservasAPIRouter = require('./routes/api/reservas');
// Importo el router de la mecánica tokens. 
var tokenRouter = require("./routes/tokens");

// Cargamos el express para su uso. 
var app = express();

// Le decimos que debe hacer y usar con las sesiones. 
app.use(session({

  cookie: {magAge: 240*60*60*1000}, //Tiempo en milisegundos

  store: store,

  saveUninitialized: true,

  resave: "true",

  secret: "cualquier cosa no pasa nada 477447"

}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Usamos passport
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

/* ------------------------------------------------------- Enrutadores con las sesiones. -------------------------------------------------------*/

// Vista del login. 
app.get('/login', function(req, res) {

  res.render("session/login");

});

// Envío del formulario del login. 
app.post('/login', function(req, res, next) {

  passport.authenticate("local", function (err, usuario, info) {

    if(err) return next(err);

    if (!usuario) return res.render("session/login", {info});

    req.logIn(usuario, function(err) {

      if(err) return next(err);

      return res.redirect('/');

    });

  })(req, res, next);

});

// Redirección del logout
app.get('/logout', function (req, res) {

  req.logOut(); // Limpiamos la sesión

  res.redirect('/');

});

// Vista del formulario de recuperación password
app.get('/forgotPassword', function(req, res) {

  res.render('session/forgotPassword');

});

// Envío del formulario de recuperación password
app.post('/forgotPassword', function(req, res) {

  Usuario.findOne({email: req.body.email}, function(err, usuario) {

    if(!usuario) return res.render('session/forgotPassword', {info: {message: "No existe ese email en nuestra BBDD."}});


    usuario.resetPassword(function(err) {

      if(err) return next(err);

      console.log("session/forgotPasswordMessage");

    });

    res.render('session/forgotPasswordMessage');

  });

});

// Reinicio de los token. 
app.get('/resetPassword/:token', function (req, res, next) {

  Token.findOne({token: req.params.token}, function(err, token) {

    if(!token) return res.status(400).send({type: "not-verified", msg: "No existe un usuario asociado al token. Verifique que su token no haya expirado."});

    Usuario.findById(token._userId, function(err, usuario) {

      if (!usuario) return res.status(400).send({msg: "No existe un usuario asociado al token."});

      res.render("session/resetPassword", {errors: {}, usuario: usuario});

    });

  });

});

// Envío del reinicio contraseña comprobando las contraseñas.
app.post('/resetPassword', function(req, res) {

  if(req.body.password != req.body.confirm_password) {

    res.render("session/resetPassword", {errors: {confirm_password: {message: "No coincide con el password introducido."}}, 
    
    usuario: new Usuario({email: req.body.email})});

    return;

  }

  Usuario.findOne({email: req.body.email}, function(err, usuario) {

    usuario.password = req.body.password;

    usuario.save(function(err) {

      if(err) {

        res.render('session/resetPassword', {errors: err.errors, usuario: new Usuario({email: req.body.email})});

      }else {

        res.redirect('/login');

      }

    });

  });

});

// Uso el router importado.
app.use('/restaurantes', loggedIn,  restaurantesRouter);
// Uso el router de la API de restaurantes.
app.use('/api/restaurantes', restaurantesAPIRouter);
// Uso el router de la API de usuarios.
app.use('/api/usuarios', usuariosAPIRouter);
// Uso el router de la API de reservas.
app.use('/api/reservas', reservasAPIRouter);
// Uso del router del token. 
app.use("/token", tokenRouter);
// Uso del router del token. 
app.use("/usuarios", usuariosRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Comprueba si un usuario está logeado o no. 
function loggedIn(req, res, next) {

  if(req.user) {

    next();

  }else {

    console.log("Usuario no logueado");

    res.redirect('/login');

  }

}

module.exports = app;
