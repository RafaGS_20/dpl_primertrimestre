let Restaurante = require("../models/Restaurante");

exports.restaurante_list = function(req, res) {

    Restaurante.allRestaurante(function(err, result) {
        
        if (err) res.status(500).send(err.message);

        res.render("restaurantes/index", {restaurantes: result});
    });
}

// Obtiene el formulario de inserción de nuevas restaurantes
exports.restaurante_create_get = function (req, res) {
    res.render("restaurantes/create");
}

exports.restaurante_create_post = function(req, res) {
      
    let restaurante = new Restaurante({

        restauranteID: req.body.id,

        nombre: req.body.nombre,

        horario: req.body.horario,

        tipocomida: req.body.tipocomida,

        telefono: req.body.telefono,

        ubicacion: req.body.ubicacion

    });
    
    Restaurante.add(restaurante, function (err, restaurante) {
        
        if (err) res.status(500).send(err.message);
        
        res.status(201).redirect("/restaurantes");
    });
}

// Método que obtiene el id de una restaurante y permite borrarla. 
exports.restaurante_delete_post = function (req, res) {
     
    Restaurante.removeById(req.body.id, function (err, result) {
        // Si hay fallo lo saca. 
        if (err) res.status(500).send(err.message);
        // Redirección al index siguiendo el patrón de diseño PRG(POST-REDIRECT-GET)
        res.redirect("/restaurantes");
    });
}


exports.restaurante_update_get = function (req, res) {
    
    Restaurante.removeById(req.body._id, function (err, result) {
        
        if (err) res.status(500).send(err.message);

        res.render("restaurantes/update", {restaurantes: result});
    });
}

// Método que sube los datos del update y actualiza. 
exports.restaurante_update_post = function (req, res) {

    let consulta = req.params.id;

    console.log(req.body);
        
    Restaurante.update(idDocumento, req.body, function (err, result) {
          
        if (err) res.status(500).send(err.message);

        res.redirect("/restaurantes");
    });
}