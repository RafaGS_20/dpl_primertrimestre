
let Reserva = require("../../models/Reserva");

exports.reservas = function(req, res) {
   
    Reserva.reservas(function(err, reservas) {
        
        if(err) res.status(500).send(err.message);
        
        res.status(200).json(reservas);

    })

};