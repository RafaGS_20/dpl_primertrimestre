
let Usuario = require("../../models/Usuario");


exports.usuarios_reservar = function (req, res) {

    Usuario.findById(req.body._id, function(err, usuario) {

        if(err) res.status(500).send(err.message);
 
        console.log(usuario);

        usuario.reservar(req.body.restaurante_id, req.body.desde, req.body.hasta, req.body.personas, function(err) {

            if(err) res.status(500).send(err.message);

            console.log("Reserva confirmada.");

            res.status(200).send();

        });

    });

};