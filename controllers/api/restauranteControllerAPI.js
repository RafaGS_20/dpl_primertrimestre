let Restaurante = require("../../models/Restaurante");

exports.restaurante_list = function(req, res) {

    Restaurante.allRestaurante(function(err, restaurante) {
        
        if (err) res.status(500).send(err.message);
        
        res.status(200).json({
            restaurantes: restaurante
        });

    });

};

exports.restaurante_create = function(req, res) {
      
    let restaurante = new Restaurante({

        restauranteID: req.body.id,

        nombre: req.body.nombre,

        horario: req.body.horario,

        tipocomida: req.body.tipocomida,

        telefono: req.body.telefono,

        ubicacion: req.body.ubicacion

    });
    
    Restaurante.add(restaurante, function (err, restaurante) {
        
        if (err) res.status(500).send(err.message);
        
        res.status(201).json({
           restaurantes: restaurante
        });

    });

};


exports.restaurante_delete = function(req, res) {
    
    Restaurante.removeById(req.body._id, function (err, result) {
        
        if (err) res.status(500).send(err.message);
        
        res.status(200).send();

    });

}


exports.restaurante_update = function(req, res) {
       
        let idDocumento = req.body._id;
        
        Restaurante.update(idDocumento, req.body, function (err, result) {
              
            if (err) res.status(500).send(err.message);
            
            res.status(201).json(
                req.body
            );

        });

}
