
let Usuario = require("../models/Usuario");


// Lo que va a exportar este controlador: 
module.exports = {
    list: function(req,res,next){     
     Usuario.find({}, function(err,usuarios) { // Llamada a la función de listar usuarios. 
            if(err) res.send(500, err.message); // Si hay fallo lo saca. 
            res.render("usuarios/index", {usuarios: usuarios}); // Sino nos redirige a la vista con todos los usuarios listados. 
        });
    },
    update_get: function(req,res, next) {
        Usuario.findById(req.params.id, function (err, usuario) { // Llamada a la función para encontrar un solo usuario. 
            res.render("usuarios/update", {errors:{}, usuario: usuario}); // Te manda a la vista para actualizarlo. 
        });
    },
    update: function(req, res, next){
        let update_values = {nombre: req.body.nombre}; // Coge los datos del body. 
        Usuario.findByIdAndUpdate(req.params.id, update_values, function(err, usuario) { // Llama a la función para actualizar el usuario. 
            if (err) { // Si hay fallo: 
                console.log(err);
                res.render("usuario/update", {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})}); // Te manda a la vista con el usuario. 
            } else { // Si no hay fallo: 
                res.redirect("/usuarios"); // Te redirige a la lista de usuarios. 
                return;
            }
        });
    },
    create_get: function(req, res, next){
        res.render("usuarios/create", {errors:{}, usuario: new Usuario()}); // Te manda a la vista para crear el usuario nuevo. 
    },
    create: function(req, res, next) {
        if (req.body.password != req.body.confirm_password) { // Si se mete mal la contraseña nos lo dice: 
            res.render("usuarios/create", {errors: {confirm_password: {message: "No coincide con el password introducido."}}, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            return;
        } // Si la contraseña está correcta: 
        Usuario.create({nombre:req.body.nombre, email: req.body.email, password: req.body.password}, function(err, nuevoUsario) { // Crea un usuario nuevo. 
            if(err){ // Si no se puede crear por algún fallo: 
                res.render("usuarios/create", {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            } else { // En caso de crearse activa la función para enviarle un email de bienvenida y te redirige a la lista de usuarios. 
                nuevoUsario.enviar_email_bienvenida();
                res.redirect("/usuarios");
            }
        });
    },
    delete: function(req,res,next){ // ENcuentra el usuario por id y lo borra, si el borrado va bien nos manda a la lista de usuarios. 
        Usuario.findByIdAndDelete(req.body.id, function(err){
            if(err)
                next(err);
            else   
                res.redirect("/usuarios");
        });
    }
};