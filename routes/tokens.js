/* ------------------------------------------------------- Enrutador de tokens. -------------------------------------------------------*/

// Le decimos todo lo que vamos a necesitar.
let express = require('express');
let router = express.Router();
let tokenController = require("../controllers/token");

// Definimos las rutas a tomar y los métodos a usar. 
router.get("/confirmation/:token", tokenController.confirmationGet);

// Exportamos todo para su uso. 
module.exports = router;