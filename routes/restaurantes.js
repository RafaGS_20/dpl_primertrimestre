/* ------------------------------------------------------- Enrutador de restaurantes. -------------------------------------------------------*/


var express = require('express');
var router = express.Router();

let restauranteController = require('../controllers/restaurante');

// Cuando en la ruta pilla /restaurante activa el restaurante.list del controlador. 
router.get("/", restauranteController.restaurante_list);

// Ruta para el create, el post y el delete. 
router.get("/create", restauranteController.restaurante_create_get);
router.get("/:id/update", restauranteController.restaurante_update_get);
router.post("/create", restauranteController.restaurante_create_post);
router.post("/:id/delete", restauranteController.restaurante_delete_post);
router.post("/:id/update", restauranteController.restaurante_update_post);

// Exportamos todo. 
module.exports = router; 
