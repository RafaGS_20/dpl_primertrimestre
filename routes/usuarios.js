/* ------------------------------------------------------- Enrutador de usuarios. -------------------------------------------------------*/

// Le decimos todo lo que vamos a necesitar.
let express = require('express');
let router = express.Router();
let usuarioController = require("../controllers/usuario");


// En el usuario a secas pilla el list del controlador para mostrarlos todos. 
router.get("/", usuarioController.list);
// Definimos el resto de rutas posibles con el método a usar. 
router.get("/create", usuarioController.create_get);
router.post("/create", usuarioController.create);
router.post("/:id/delete", usuarioController.delete);
router.get("/:id/update", usuarioController.update_get);
router.post("/:id/update", usuarioController.update);

// Exportamos todo. 
module.exports = router;