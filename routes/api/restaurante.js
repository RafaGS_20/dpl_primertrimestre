/* ------------------------------------------------------- API de los restaurantes. -------------------------------------------------------*/

// Le decimos todo lo que vamos a necesitar. 
let express = require('express');
let router = express.Router();
let restauranteControllerAPI = require("../../controllers/api/restauranteControllerAPI");

// Aquí definimos las rutas a tomar para cada proceso y los métodos que usarán. 
router.get("/", restauranteControllerAPI.restaurante_list);
router.post("/create", restauranteControllerAPI.restaurante_create);
router.delete("/delete", restauranteControllerAPI.restaurante_delete);
router.put("/update", restauranteControllerAPI.restaurante_update);

// Exportamos todo. 
module.exports = router;