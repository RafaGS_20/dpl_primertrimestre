/* ------------------------------------------------------- API de los usuarios. -------------------------------------------------------*/

// Le decimos todo lo que vamos a necesitar.
let express = require('express');
let router = express.Router();
let usuariosControllerAPI = require("../../controllers/api/usuariosControllerAPI");

// Aquí definimos las rutas a tomar para cada proceso y los métodos que usarán.
router.post("/reservar", usuariosControllerAPI.usuarios_reservar);

// Exportamos todo. 
module.exports = router;