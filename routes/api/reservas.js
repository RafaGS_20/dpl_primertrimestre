/* ------------------------------------------------------- API de las reservas. -------------------------------------------------------*/

// Le decimos todo lo que vamos a necesitar.
let express = require('express');
let router = express.Router();
let reservasControllerAPI = require('../../controllers/api/reservasControllerApi');

// Aquí definimos las rutas a tomar para cada proceso y los métodos que usarán. 
router.get('/reservas', reservasControllerAPI.reservas);

// Exportamos todo. 
module.exports = router;